package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountChangeSuite2 extends FunSuite {
  import Main.countChange2
  test("countChange: example given in instructions") {
    assert(countChange2(4,List(1,2)) === 3)
  }

  test("countChange: sorted CHF") {
    assert(countChange2(300,List(5,10,20,50,100,200,500)) === 1022)
  }

  test("countChange: no pennies") {
    assert(countChange2(301,List(5,10,20,50,100,200,500)) === 0)
  }

  test("countChange: unsorted CHF") {
    assert(countChange2(300,List(500,5,50,100,20,200,10)) === 1022)
  }
}
