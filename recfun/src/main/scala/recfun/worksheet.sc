package recfun

object worksheet {

  // exercise 1 - pascal's triangle
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }                                               //> pascal: (c: Int, r: Int)Int

  pascal(0, 2)                                    //> res0: Int = 1
  pascal(1, 2)                                    //> res1: Int = 2
  pascal(1, 3)                                    //> res2: Int = 3

  // exercise 2 - parenthesis balancing
  def balance(chars: List[Char]): Boolean = {

    def doCount(count: Int, chars: List[Char]): Int = {
      if (count < 0 || chars.isEmpty) count
      else doCount(
        chars.head match {
          case '(' => count + 1
          case ')' => count - 1
          case _ => count
        }, chars.tail)
    }

    doCount(0, chars) == 0
  }                                               //> balance: (chars: List[Char])Boolean

  balance("(if (zero? x) max (/ 1 x))".toList)    //> res3: Boolean = true
  balance("I told him (that it's not (yet) done). (But he wasn't listening)".toList)
                                                  //> res4: Boolean = true
  balance(":-)".toList)                           //> res5: Boolean = false
  balance("())(".toList)                          //> res6: Boolean = false

  // exercise 3 - counting change
  def countChange(money: Int, coins: List[Int]): Int = {
    def count(money: Int, coin: Int): Int = {
      if (money == 0) 1
      else if (money < 0) 0
      else if (coin < 0 && money > 0) 0
      else count(money, coin - 1) + count(money - coins(coin), coin)
    }

    count(money, coins.length - 1)
  }                                               //> countChange: (money: Int, coins: List[Int])Int

  countChange(4, List(1, 2))                      //> res7: Int = 3
}