package recfun

object worksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(190); 

  // exercise 1 - pascal's triangle
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  };System.out.println("""pascal: (c: Int, r: Int)Int""");$skip(16); val res$0 = 

  pascal(0, 2);System.out.println("""res0: Int = """ + $show(res$0));$skip(15); val res$1 = 
  pascal(1, 2);System.out.println("""res1: Int = """ + $show(res$1));$skip(15); val res$2 = 
  pascal(1, 3);System.out.println("""res2: Int = """ + $show(res$2));$skip(386); 

  // exercise 2 - parenthesis balancing
  def balance(chars: List[Char]): Boolean = {

    def doCount(count: Int, chars: List[Char]): Int = {
      if (count < 0 || chars.isEmpty) count
      else doCount(
        chars.head match {
          case '(' => count + 1
          case ')' => count - 1
          case _ => count
        }, chars.tail)
    }

    doCount(0, chars) == 0
  };System.out.println("""balance: (chars: List[Char])Boolean""");$skip(48); val res$3 = 

  balance("(if (zero? x) max (/ 1 x))".toList);System.out.println("""res3: Boolean = """ + $show(res$3));$skip(85); val res$4 = 
  balance("I told him (that it's not (yet) done). (But he wasn't listening)".toList);System.out.println("""res4: Boolean = """ + $show(res$4));$skip(24); val res$5 = 
  balance(":-)".toList);System.out.println("""res5: Boolean = """ + $show(res$5));$skip(25); val res$6 = 
  balance("())(".toList);System.out.println("""res6: Boolean = """ + $show(res$6));$skip(345); 

  // exercise 3 - counting change
  def countChange(money: Int, coins: List[Int]): Int = {
    def count(money: Int, coin: Int): Int = {
      if (money == 0) 1
      else if (money < 0) 0
      else if (coin < 0 && money > 0) 0
      else count(money, coin - 1) + count(money - coins(coin), coin)
    }

    count(money, coins.length - 1)
  };System.out.println("""countChange: (money: Int, coins: List[Int])Int""");$skip(30); val res$7 = 

  countChange(4, List(1, 2));System.out.println("""res7: Int = """ + $show(res$7))}
}
