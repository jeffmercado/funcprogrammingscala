package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
    val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
  }

  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a', 'b', 'd'))
    }
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }

  test("times(\"hello, world\")") {
    val results = times(string2Chars("hello, world"))
    assert(results.contains(('h', 1)))
    assert(results.contains(('e', 1)))
    assert(results.contains(('l', 3)))
    assert(results.contains(('o', 2)))
    assert(results.contains(('w', 1)))
    assert(results.contains(('r', 1)))
    assert(results.contains(('d', 1)))
  }

  test("singleton") {
    assert(singleton(List(Leaf('e', 1), Leaf('t', 2))) == false)
    assert(singleton(List(Leaf('e', 1))) == true)
    assert(singleton(Nil) == false)
  }

  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4)))
  }

  test("combine of some leaf list - size 4") {
    val leaflist = List(Leaf('a', 1), Leaf('b', 1), Leaf('c', 1), Leaf('d', 1))
    assert(combine(leaflist) === List(Leaf('c', 1), Leaf('d', 1), Fork(Leaf('a', 1), Leaf('b', 1), List('a', 'b'), 2)))
  }

  test("combine of some leaf list - size 5") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 1), Leaf('y', 5), Leaf('z', 6))
    assert(combine(leaflist) === List(Leaf('x', 1), Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('y', 5), Leaf('z', 6)))
  }

  test("until some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(until(singleton, combine)(leaflist) === List(Fork(Fork(Leaf('e', 1), Leaf('t', 2), List('e', 't'), 3), Leaf('x', 4), List('e', 't', 'x'), 7)))
  }

  test("create code tree") {
    val str = "Hello World!"
    assert(createCodeTree(string2Chars("hello, world")) === Fork(Fork(Leaf('o',2),Fork(Leaf('h',1),Fork(Leaf(',',1),Leaf('e',1),List(',', 'e'),2),List('h', ',', 'e'),3),List('o', 'h', ',', 'e'),5),Fork(Leaf('l',3),Fork(Fork(Leaf('w',1),Leaf(' ',1),List('w', ' '),2),Fork(Leaf('d',1),Leaf('r',1),List('d', 'r'),2),List('w', ' ', 'd', 'r'),4),List('l', 'w', ' ', 'd', 'r'),7),List('o', 'h', ',', 'e', 'l', 'w', ' ', 'd', 'r'),12))
  }

  test("decode") {
    new TestTrees {
      assert(decode(t1, List(0, 1, 0, 1, 1)) === "ababb".toList)
      assert(decode(t2, List(1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1)) === "dabdabdbd".toList)
      intercept[Exception] { decode(t2, List(1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0)) }
    }
  }

  test("decoded secret") {
    new TestTrees {
      assert(decodedSecret === "huffmanestcool".toList)
    }
  }

  test("encode") {
    new TestTrees {
      assert(encode(t1)("ababb".toList) === List(0, 1, 0, 1, 1))
      assert(encode(t2)("dabdabdbd".toList) === List(1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1))
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("convert T1") {
    new TestTrees {
      val codeTableT1 = convert(t1)
      assert(codeTableT1.length === 2)
      assert(codeTableT1.contains(('a', List(0))))
      assert(codeTableT1.contains(('b', List(1))))
    }
  }
  
  test("convert T2") {
    new TestTrees {
      val codeTableT2 = convert(t2)
      assert(codeTableT2.length === 3)
      assert(codeTableT2.contains(('a', List(0, 0))))
      assert(codeTableT2.contains(('b', List(0, 1))))
      assert(codeTableT2.contains(('d', List(1))))
    }
  }  
}
