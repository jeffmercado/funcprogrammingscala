package pouring


object ws {
	val problem = new Pouring(Vector(4, 7, 19))
                                                  //> problem  : pouring.Pouring = pouring.Pouring@424ecfdd
	problem.moves                             //> res0: scala.collection.immutable.IndexedSeq[Product with Serializable with po
                                                  //| uring.ws.problem.Move] = Vector(Empty(0), Empty(1), Empty(2), Fill(0), Fill(1
                                                  //| ), Fill(2), Pour(0,0), Pour(0,1), Pour(0,2), Pour(1,1), Pour(1,2), Pour(2,2))
                                                  //| 
  problem.solutions(6)                            //> res1: Stream[pouring.ws.problem.Path] = Stream(Fill(1) Fill(0) Pour(1,2) Fil
                                                  //| l(1) Pour(2,2) Pour(1,2) Pour(0,1)--> Vector(0, 6, 19), ?)
 
 
}