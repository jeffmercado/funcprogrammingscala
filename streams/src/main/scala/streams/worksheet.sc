package streams

object worksheet {

	val test = Bloxorz.Level1                 //> test  : streams.Bloxorz.Level1.type = streams.Bloxorz$Level1$@11bbf1ca
		
	test.pathsFromStart.toList take 10        //> res0: List[(streams.worksheet.test.Block, List[streams.worksheet.test.Move])
                                                  //| ] = List((Block(Pos(1,1),Pos(1,1)),List()), (Block(Pos(1,1),Pos(1,1)),List()
                                                  //| ))
	  
	
  test.pathsToGoal                                //> res1: Stream[(streams.worksheet.test.Block, List[streams.worksheet.test.Move
                                                  //| ])] = Stream()

  println(test.solution)                          //> List()
}