package objsets

object worksheet {

  val google = GoogleVsApple.googleTweets         //> google  : objsets.TweetSet = objsets.NonEmpty@236527f
  val apple = GoogleVsApple.appleTweets           //> apple  : objsets.TweetSet = objsets.NonEmpty@11e7c5cb
  val all = google.union(apple)                   //> all  : objsets.TweetSet = objsets.NonEmpty@76a9b9c
  all.mostRetweeted                               //> res0: objsets.Tweet = User: gizmodo
                                                  //| Text: iPhone 5's brain dissected. Guess what, it's made by Samsung. http://t
                                                  //| .co/wSyjvpDc [321]
  apple.mostRetweeted                             //> res1: objsets.Tweet = User: gizmodo
                                                  //| Text: iPhone 5's brain dissected. Guess what, it's made by Samsung. http://t
                                                  //| .co/wSyjvpDc [321]
  google.mostRetweeted                            //> res2: objsets.Tweet = User: gizmodo
                                                  //| Text: Warning: Security bug can wipe out your Android phone just by visiting
                                                  //|  a web page-not only limited to Samsung http://t.co/0y6vnOKw [290]
  apple.descendingByRetweet foreach println       //> User: gizmodo
                                                  //| Text: iPhone 5's brain dissected. Guess what, it's made by Samsung. http://t
                                                  //| .co/wSyjvpDc [321]
                                                  //| User: mashable
                                                  //| Text: Our iPhones Are Depleting the Earth's Resources [INFOGRAPHIC] http://t
                                                  //| .co/XnTLqe0p [205]
                                                  //| User: gizmodo
                                                  //| Text: The weirdest thing people hate about the iPhone 5: http://t.co/GMwuRp8
                                                  //| D [202]
                                                  //| User: mashable
                                                  //| Text: iPad 4 Has Carbon Fiber Body, Flexible Display [REPORT] http://t.co/Df
                                                  //| t5VoXD via @tabtimes [198]
                                                  //| User: gizmodo
                                                  //| Text: The definitive comparison of iOS 5 Google Maps vs iOS 6 Apple Maps in 
                                                  //| one single image: http://t.co/fTwTfVMy [191]
                                                  //| User: mashable
                                                  //| Text: iOS 6 Users Complain About Wi-Fi, Connectivity Issues - http://t.co/io
                                                  //| gRstNn [180]
                                                  //| User: CNET
                                                  //| Text: RT @CNETNews: Apple "fell short" with iOS 6 maps, and we are "extremel
                                                  //| y sorry," CEO Tim Cook says in open letter http://t.co/t1U4497r [139]
                                                  //| User: CNET
                                                  //| Text: How to switch from iPhone to Android http://t.co/M8I9
                                                  //| Output exceeds cutoff limit.
  google.descendingByRetweet foreach println      //> User: gizmodo
                                                  //| Text: Warning: Security bug can wipe out your Android phone just by visiting
                                                  //|  a web page-not only limited to Samsung http://t.co/0y6vnOKw [290]
                                                  //| User: CNET
                                                  //| Text: How to switch from iPhone to Android http://t.co/M8I9lwua [131]
                                                  //| User: gizmodo
                                                  //| Text: Major Samsung security bug can wipe your Galaxy phone (updating) http:
                                                  //| //t.co/n5yDZ3dh [120]
                                                  //| User: gizmodo
                                                  //| Text: iPhone 5 vs Galaxy S III: Who's screen is prettier? http://t.co/n6Cbas
                                                  //| pY [108]
                                                  //| User: gizmodo
                                                  //| Text: This $50 stick turns any HDTV into an Android-powered smart TV: http:/
                                                  //| /t.co/8FpZUnIE [101]
                                                  //| User: mashable
                                                  //| Text: Smartphone Camera Shootout: iPhone 5 vs. Galaxy SIII vs. iPhone 4S htt
                                                  //| p://t.co/Qp0PM0fh [85]
                                                  //| User: gadgetlab
                                                  //| Text: First iPhone 5 Benchmarks: Screaming Fast, Yes, But Just Shy of Galaxy
                                                  //|  S III  http://t.co/QIAhda3L by @redgirlsays [79]
                                                  //| User: mashable
                                                  //| Text: 5 Mobile Photographers Capturing the World With #Android http://t.co/
                                                  //| Output exceeds cutoff limit.

}