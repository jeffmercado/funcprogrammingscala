package funsets

import scala.math._
import FunSets._

object worksheet {
  val s1 = singletonSet(1)                        //> s1  : funsets.FunSets.Set = <function1>
  val s2 = singletonSet(2)                        //> s2  : funsets.FunSets.Set = <function1>
  val ms1 = Set(2, 4, 6)                          //> ms1  : scala.collection.immutable.Set[Int] = Set(2, 4, 6)
  val ms2 = Set(1, 3, 5)                          //> ms2  : scala.collection.immutable.Set[Int] = Set(1, 3, 5)
	val fas1 = Set(-1000, -500, 250, 0, 200, 400, 800) // all even
                                                  //> fas1  : scala.collection.immutable.Set[Int] = Set(0, -1000, -500, 250, 800, 
                                                  //| 400, 200)
	val fas2 = Set(-999, -455, 33, 1, 79, 399, 901) // all odd
                                                  //> fas2  : scala.collection.immutable.Set[Int] = Set(-455, 1, 33, 901, -999, 39
                                                  //| 9, 79)
	val fas3 = Set(-900, -800, -322, -135) // all negative
                                                  //> fas3  : scala.collection.immutable.Set[Int] = Set(-900, -800, -322, -135)
	val fas4 = Set(-10000, -1002, -1000, -400, 2, 300, 1001, 2000) // has out of bounds
                                                  //> fas4  : scala.collection.immutable.Set[Int] = Set(1001, 2000, -1000, 2, -400
                                                  //| , 300, -10000, -1002)
 
  val u = union(s1, s2)                           //> u  : funsets.FunSets.Set = <function1>
  val i = intersect(s1, s2)                       //> i  : funsets.FunSets.Set = <function1>
  val d = diff(s1, s2)                            //> d  : funsets.FunSets.Set = <function1>
  val f = filter(u, x => x % 2 == 0)              //> f  : funsets.FunSets.Set = <function1>
  
  contains(u, 1)                                  //> res0: Boolean = true
  contains(i, 1)                                  //> res1: Boolean = false
  contains(d, 1)                                  //> res2: Boolean = true
  contains(f, 2)                                  //> res3: Boolean = true
  
  
  def isEven(i: Int): Boolean = i % 2 == 0        //> isEven: (i: Int)Boolean
  forall(ms1, isEven)                             //> res4: Boolean = true
  forall(ms2, isEven)                             //> res5: Boolean = false
  forall(fas1, isEven)                            //> res6: Boolean = true
  forall(fas2, isEven)                            //> res7: Boolean = false
  forall(fas3, isEven)                            //> res8: Boolean = false
  forall(fas4, isEven)                            //> res9: Boolean = true
  
  def isOdd(i: Int): Boolean = i % 2 != 0         //> isOdd: (i: Int)Boolean
  forall(fas1, isOdd)                             //> res10: Boolean = false
  forall(fas2, isOdd)                             //> res11: Boolean = true
  forall(fas3, isOdd)                             //> res12: Boolean = false
  forall(fas4, isOdd)                             //> res13: Boolean = false
}