package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
    val s5 = singletonSet(5)
    val n1 = singletonSet(-1)
    val n500 = singletonSet(-500)
    val n1001 = singletonSet(-1001)

    val ms1 = union(s1, union(s2, s3))
    val ms2 = union(s3, union(s4, s5))

    val fas1 = union(n500, union(s2, s4)) // all even
    val fas2 = union(n1, union(s1, union(s3, s5))) // all odd
    val fas3 = union(n1, n500) // all negative
    val fas4 = union(n1001, fas1) // has some out of bounds

    def isEven(i: Int): Boolean = i % 2 == 0
    def isOdd(i: Int): Boolean = i % 2 != 0
    def isNegative(i: Int): Boolean = i < 0
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  // 2.1.2 - union/intersect/diff //////////////////////////////

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersect contains only overlapping elements") {
    new TestSets {
      val s = intersect(ms1, ms2)
      assert(!contains(s, 1), "Intersect 1")
      assert(!contains(s, 2), "Intersect 2")
      assert(contains(s, 3), "Intersect 3")
      assert(!contains(s, 4), "Intersect 4")
      assert(!contains(s, 5), "Intersect 5")
    }
  }

  test("diff contains elements in ms1 not in ms2") {
    new TestSets {
      val s = diff(ms1, ms2)
      assert(contains(s, 1), "Diff 1")
      assert(contains(s, 2), "Diff 2")
      assert(!contains(s, 3), "Diff 3")
      assert(!contains(s, 4), "Diff 4")
      assert(!contains(s, 5), "Diff 5")
    }
  }

  // 2.1.3 - filter //////////////////////////////

  test("filter even numbers") {
    new TestSets {
      val s = union(ms1, ms2)
      val f = filter(s, x => x % 2 == 0)
      assert(!contains(f, 1), "Filter 1")
      assert(contains(f, 2), "Filter 2")
      assert(!contains(f, 3), "Filter 3")
      assert(contains(f, 4), "Filter 4")
      assert(!contains(f, 5), "Filter 5")
    }
  }

  // 2.2.1 - forall /////////////////////////////////

  test("all bounded numbers in set are even") {
    new TestSets {
      assert(forall(fas1, isEven), "Even Numbers 1")
      assert(!forall(fas2, isEven), "Even Numbers 2")
      assert(!forall(fas3, isEven), "Even Numbers 3")
      assert(forall(fas4, isEven), "Even Numbers 4")
    }
  }

  test("all bounded numbers in set are odd") {
    new TestSets {
      assert(!forall(fas1, isOdd), "Odd Numbers 1")
      assert(forall(fas2, isOdd), "Odd Numbers 2")
      assert(!forall(fas3, isOdd), "Odd Numbers 3")
      assert(!forall(fas4, isOdd), "Odd Numbers 4")
    }
  }

  test("all bounded numbers in set are negative") {
    new TestSets {
      assert(!forall(fas1, isNegative), "Negative Numbers 1")
      assert(!forall(fas2, isNegative), "Negative Numbers 2")
      assert(forall(fas3, isNegative), "Negative Numbers 3")
      assert(!forall(fas4, isNegative), "Negative Numbers 4")
    }
  }

  // 2.2.2 - exists /////////////////////////////////

  test("exists at least 1 bounded number in set that is even") {
    new TestSets {
      assert(exists(fas1, isEven), "Even Numbers 1")
      assert(!exists(fas2, isEven), "Even Numbers 2")
      assert(exists(fas3, isEven), "Even Numbers 3")
      assert(exists(fas4, isEven), "Even Numbers 4")
    }
  }

  test("exists at least 1 number in set that is odd") {
    new TestSets {
      assert(!exists(fas1, isOdd), "Odd Numbers 1")
      assert(exists(fas2, isOdd), "Odd Numbers 2")
      assert(exists(fas3, isOdd), "Odd Numbers 3")
      assert(!exists(fas4, isOdd), "Odd Numbers 4")
    }
  }

  test("exists at least 1 number in set that is negative") {
    new TestSets {
      assert(exists(fas1, isNegative), "Negative Numbers 1")
      assert(exists(fas2, isNegative), "Negative Numbers 2")
      assert(exists(fas3, isNegative), "Negative Numbers 3")
      assert(exists(fas4, isNegative), "Negative Numbers 4")
    }
  }

  // 2.2.3 - map /////////////////////////////////

  test("map all numbers to negative") {
    new TestSets {
      val negation = map(fas1, x => -x)
      assert(negation(500) && negation(-2) && negation(-4), "Map (to negative)")
      assert(!negation(-500) && !negation(2) && !negation(4), "Map (to negative)")

      val square = map(fas1, x => x * x)
      assert(square(250000) && square(4) && square(16), "Map (squares)")
      assert(!square(-250000) && !square(-4) && !square(-16), "Map (squares)")
    }
  }
}
