package funsets

import scala.math._
import FunSets._

object worksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(100); 
  val s1 = singletonSet(1);System.out.println("""s1  : funsets.FunSets.Set = """ + $show(s1 ));$skip(27); 
  val s2 = singletonSet(2);System.out.println("""s2  : funsets.FunSets.Set = """ + $show(s2 ));$skip(25); 
  val ms1 = Set(2, 4, 6);System.out.println("""ms1  : scala.collection.immutable.Set[Int] = """ + $show(ms1 ));$skip(25); 
  val ms2 = Set(1, 3, 5);System.out.println("""ms2  : scala.collection.immutable.Set[Int] = """ + $show(ms2 ));$skip(64); 
	val fas1 = Set(-1000, -500, 250, 0, 200, 400, 800);System.out.println("""fas1  : scala.collection.immutable.Set[Int] = """ + $show(fas1 ));$skip(60);  // all even
	val fas2 = Set(-999, -455, 33, 1, 79, 399, 901);System.out.println("""fas2  : scala.collection.immutable.Set[Int] = """ + $show(fas2 ));$skip(56);  // all odd
	val fas3 = Set(-900, -800, -322, -135);System.out.println("""fas3  : scala.collection.immutable.Set[Int] = """ + $show(fas3 ));$skip(85);  // all negative
	val fas4 = Set(-10000, -1002, -1000, -400, 2, 300, 1001, 2000);System.out.println("""fas4  : scala.collection.immutable.Set[Int] = """ + $show(fas4 ));$skip(26);  // has out of bounds
 
  val u = union(s1, s2);System.out.println("""u  : funsets.FunSets.Set = """ + $show(u ));$skip(28); 
  val i = intersect(s1, s2);System.out.println("""i  : funsets.FunSets.Set = """ + $show(i ));$skip(23); 
  val d = diff(s1, s2);System.out.println("""d  : funsets.FunSets.Set = """ + $show(d ));$skip(37); 
  val f = filter(u, x => x % 2 == 0);System.out.println("""f  : funsets.FunSets.Set = """ + $show(f ));$skip(20); val res$0 = 
  
  contains(u, 1);System.out.println("""res0: Boolean = """ + $show(res$0));$skip(17); val res$1 = 
  contains(i, 1);System.out.println("""res1: Boolean = """ + $show(res$1));$skip(17); val res$2 = 
  contains(d, 1);System.out.println("""res2: Boolean = """ + $show(res$2));$skip(17); val res$3 = 
  contains(f, 2);System.out.println("""res3: Boolean = """ + $show(res$3));$skip(49); 
  
  
  def isEven(i: Int): Boolean = i % 2 == 0;System.out.println("""isEven: (i: Int)Boolean""");$skip(22); val res$4 = 
  forall(ms1, isEven);System.out.println("""res4: Boolean = """ + $show(res$4));$skip(22); val res$5 = 
  forall(ms2, isEven);System.out.println("""res5: Boolean = """ + $show(res$5));$skip(23); val res$6 = 
  forall(fas1, isEven);System.out.println("""res6: Boolean = """ + $show(res$6));$skip(23); val res$7 = 
  forall(fas2, isEven);System.out.println("""res7: Boolean = """ + $show(res$7));$skip(23); val res$8 = 
  forall(fas3, isEven);System.out.println("""res8: Boolean = """ + $show(res$8));$skip(23); val res$9 = 
  forall(fas4, isEven);System.out.println("""res9: Boolean = """ + $show(res$9));$skip(45); 
  
  def isOdd(i: Int): Boolean = i % 2 != 0;System.out.println("""isOdd: (i: Int)Boolean""");$skip(22); val res$10 = 
  forall(fas1, isOdd);System.out.println("""res10: Boolean = """ + $show(res$10));$skip(22); val res$11 = 
  forall(fas2, isOdd);System.out.println("""res11: Boolean = """ + $show(res$11));$skip(22); val res$12 = 
  forall(fas3, isOdd);System.out.println("""res12: Boolean = """ + $show(res$12));$skip(22); val res$13 = 
  forall(fas4, isOdd);System.out.println("""res13: Boolean = """ + $show(res$13))}
}
