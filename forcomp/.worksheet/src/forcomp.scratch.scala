package forcomp

import Anagrams._;

object scratch {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(82); 
  val list1 = List(('a', 1));System.out.println("""list1  : List[(Char, Int)] = """ + $show(list1 ));$skip(39); 
  val list2 = List(('a', 2), ('b', 1));System.out.println("""list2  : List[(Char, Int)] = """ + $show(list2 ));$skip(49); 
  val list3 = List(('a', 2), ('b', 2), ('c', 2));System.out.println("""list3  : List[(Char, Int)] = """ + $show(list3 ));$skip(37); 
  val diff1 = subtract(list3, list1);System.out.println("""diff1  : forcomp.Anagrams.Occurrences = """ + $show(diff1 ));$skip(26); 

  val map1 = list2.toMap;System.out.println("""map1  : scala.collection.immutable.Map[Char,Int] = """ + $show(map1 ));$skip(25); 
  val map2 = list1.toMap;System.out.println("""map2  : scala.collection.immutable.Map[Char,Int] = """ + $show(map2 ));$skip(218); 
  def sub(x: Occurrences, y: Occurrences): Occurrences = {
    y.foldLeft(x.toMap)((xMap, a) => a match {
    	case (c, i) =>
    		if (xMap(c) == i) xMap - c
    		else xMap updated (c, xMap(c) - i)
    }).toList
  };System.out.println("""sub: (x: forcomp.Anagrams.Occurrences, y: forcomp.Anagrams.Occurrences)forcomp.Anagrams.Occurrences""");$skip(23); val res$0 = 
  
  sub(list3, list1);System.out.println("""res0: forcomp.Anagrams.Occurrences = """ + $show(res$0))}

}
