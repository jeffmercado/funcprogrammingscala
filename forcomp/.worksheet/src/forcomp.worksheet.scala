package forcomp
import Anagrams._;

object worksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(95); 

  val sentence = List("Linux", "rulez");System.out.println("""sentence  : List[String] = """ + $show(sentence ));$skip(398); 
  def anagrams(sentence: Sentence): List[Sentence] = {
    def iter(occurrences: Occurrences): List[Sentence] =
      if (occurrences.isEmpty) List(Nil)
      else
        for {
          combo <- combinations(occurrences)
          word <- dictionaryByOccurrences(combo)
          tail <- iter(subtract(occurrences, combo))
        } yield word :: tail
    iter(sentenceOccurrences(sentence))
  };System.out.println("""anagrams: (sentence: forcomp.Anagrams.Sentence)List[forcomp.Anagrams.Sentence]""");$skip(22); val res$0 = 

  anagrams(sentence);System.out.println("""res0: List[forcomp.Anagrams.Sentence] = """ + $show(res$0))}
}
  