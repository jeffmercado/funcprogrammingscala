package forcomp
import Anagrams._;

object worksheet {

  val sentence = List("Linux", "rulez")           //> sentence  : List[String] = List(Linux, rulez)
  def anagrams(sentence: Sentence): List[Sentence] = {
    def iter(occurrences: Occurrences): List[Sentence] =
      if (occurrences.isEmpty) List(Nil)
      else
        for {
          combo <- combinations(occurrences)
          word <- dictionaryByOccurrences(combo)
          tail <- iter(subtract(occurrences, combo))
        } yield word :: tail
    iter(sentenceOccurrences(sentence))
  }                                               //> anagrams: (sentence: forcomp.Anagrams.Sentence)List[forcomp.Anagrams.Sentenc
                                                  //| e]

  anagrams(sentence)                              //> res0: List[forcomp.Anagrams.Sentence] = List(List(Lin, Rex, Zulu), List(Lin,
                                                  //|  Zulu, Rex), List(nil, Rex, Zulu), List(nil, Zulu, Rex), List(null, Rex, Uzi
                                                  //| ), List(null, Uzi, Rex), List(Rex, Lin, Zulu), List(Rex, nil, Zulu), List(Re
                                                  //| x, null, Uzi), List(Rex, Uzi, null), List(Rex, Zulu, Lin), List(Rex, Zulu, n
                                                  //| il), List(Linux, rulez), List(Uzi, null, Rex), List(Uzi, Rex, null), List(ru
                                                  //| lez, Linux), List(Zulu, Lin, Rex), List(Zulu, nil, Rex), List(Zulu, Rex, Lin
                                                  //| ), List(Zulu, Rex, nil))
}
  