package forcomp

import Anagrams._;

object scratch {
  val list1 = List(('a', 1))                      //> list1  : List[(Char, Int)] = List((a,1))
  val list2 = List(('a', 2), ('b', 1))            //> list2  : List[(Char, Int)] = List((a,2), (b,1))
  val list3 = List(('a', 2), ('b', 2), ('c', 2))  //> list3  : List[(Char, Int)] = List((a,2), (b,2), (c,2))
  val diff1 = subtract(list3, list1)              //> diff1  : forcomp.Anagrams.Occurrences = List((a,1), (b,2), (c,2))

  val map1 = list2.toMap                          //> map1  : scala.collection.immutable.Map[Char,Int] = Map(a -> 2, b -> 1)
  val map2 = list1.toMap                          //> map2  : scala.collection.immutable.Map[Char,Int] = Map(a -> 1)
  def sub(x: Occurrences, y: Occurrences): Occurrences = {
    y.foldLeft(x.toMap)((xMap, a) => a match {
    	case (c, i) =>
    		if (xMap(c) == i) xMap - c
    		else xMap updated (c, xMap(c) - i)
    }).toList
  }                                               //> sub: (x: forcomp.Anagrams.Occurrences, y: forcomp.Anagrams.Occurrences)forco
                                                  //| mp.Anagrams.Occurrences
  
  sub(list3, list1)                               //> res0: forcomp.Anagrams.Occurrences = List((a,1), (b,2), (c,2))

}